<?php

use Illuminate\Http\Request;

#Hero Routes
Route::get('hero_races', 'HeroRaceController@index');
Route::get('hero_classes', 'HeroClassController@index');
Route::get('hero_weapons', 'HeroWeaponController@index');
Route::get('hero_first_names', 'HeroFirstNameController@index');
Route::get('hero_last_names', 'HeroLastNameController@index');

Route::get('heroes', 'HeroController@index');
Route::get('heroes/{id}', 'HeroController@show');

Route::post('heroes', 'HeroController@store');
Route::put('heroes/{id}', 'HeroController@update');
Route::delete('heroes/{id}', 'HeroController@destroy');
#Random Hero
Route::post('hero_generate_random', 'HeroController@generateRandom');

#Number of heroes and popular data
Route::get('heroes_total', 'HeroController@totalHeroes');
Route::get('heroes_popular_race', 'HeroController@popularRace');
Route::get('heroes_popular_class', 'HeroController@popularClass');
Route::get('heroes_popular_weapon', 'HeroController@popularWeapon');


#Monster Routes
Route::get('monster_races', 'MonsterRaceController@index');
Route::get('monster_powers', 'MonsterPowerController@index');

Route::get('monsters', 'MonsterController@index');
Route::get('monsters/{id}', 'MonsterController@show');

Route::post('monsters', 'MonsterController@store');
Route::put('monsters/{id}', 'MonsterController@update');
Route::delete('monsters/{id}', 'MonsterController@destroy');
#Random Hero
Route::post('monster_generate_random', 'MonsterController@generateRandom');

#Number of monster and popular data
Route::get('monsters_total', 'MonsterController@totalMonsters');
Route::get('monsters_popular_race', 'MonsterController@popularRace');
Route::get('monsters_popular_ability', 'MonsterController@popularAbility');