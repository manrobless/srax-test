<?php

namespace App\Http\Controllers;

use App\HeroRace;
use Illuminate\Http\Request;

class HeroRaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return HeroRace::all();
    }

}
