<?php

namespace App\Http\Controllers;

use App\HeroWeapon;
use Illuminate\Http\Request;

class HeroWeaponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return HeroWeapon::all();
    }

}
