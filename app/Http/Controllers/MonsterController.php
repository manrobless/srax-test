<?php

namespace App\Http\Controllers;

use App\Monster;
use Illuminate\Http\Request;
use App\MonsterPower;
use App\MonsterRace;
use App\MonsterAbility;

class MonsterController extends Controller
{
    #Display a listing of Monsters
    public function index()
    {
        return Monster::all();
    }

    #Create a Monster
    public function store(Request $request)
    {
        $validMonster = $this->validateMonster($request->all(), true);

        if ($validMonster === true){
            #Save the Monster
            $monster =  Monster::create($request->all());

            $abilities = $request->all()["abilities"];
            foreach ($abilities as $ability)  {
                $monster_ability = new MonsterAbility;
                $monster_ability->monster_id = $monster['id'];
                $monster_ability->power_id = $ability;
                $monster_ability->save();
            }

            return $monster;

        } else{
            return response()-> json(["error" => true,"description" => $validMonster]);
        }
    }

    #Generate Random Monster
    public function generateRandom()
    {
        $rand_race_id =  rand(1,24); #There are 24 races
        $base_name = MonsterRace::find($rand_race_id )->first()["name"];

        #1.- Every A char in the name will be replaced with O
        $name = str_ireplace("A","O",$base_name);
        #2.- Every U char in the name will be replaced with A
        $name = str_ireplace("U","A",$name);
        #3.-Every I will add another I right next to it
        $name = str_ireplace("I","I",$name);
        #4.-Every E char in the name will change position with the next character onthe right
            #Not completed
        #5.- Every O char in the name will add an R on the left side of it
        $name = str_ireplace("O","RO",$name);

        $rand_abilities = [rand(1,10), rand(1,10)];
        $monster = [
            "race_id" => $rand_race_id,
            "abilities" => $rand_abilities
        ];

        // return response()-> json($rand_abilities);
        #Validate habilities
        $validAbilities = $this->validateAbilities($monster);

        while (!$validAbilities === true){
            $rand_abilities = [rand(1,10), rand(1,10)];
            $monster = [
                "race_id" => $rand_race_id,
                "abilities" => $rand_abilities
            ];
            #Try again
            $validAbilities = $this->validateAbilities($monster);
        }

        $rand_picture = []; #Not completed
        
        #Generate a random monster after validations
        $random_monster = [
            "name" => $name,
            "picture" => $rand_picture,
            "level" => 1,
            "race_id" => $rand_race_id,
            "strength" => rand(0,100),
            "intelligence" => rand(0,100),
            "dexterity" => rand(0,100),
            "abilities" => $rand_abilities
        ];

        #Return the monster data to "fill" the "random monster form" in "view"
        return response()-> json($random_monster);
    }

    #Display the specified Monster
    public function show($id)
    {
        return Monster::find($id);
    }

    #Update the Monster.
    public function update(Request $request, $id)
    {
        $validMonster = $this->validateMonster($request->all(), false);

        if ($validMonster === true){
            #Update the Monster
            $monster_data = $request->all();
            #Set level by number of abilities
            $monster_data['level'] = (count($monster_data['abilities']) * 2) - 1;
            
            $monster = Monster::findOrFail($id);
            $monster->update($monster_data);
            return $monster;

        } else{
            return response()-> json(["error" => true,"description" => $validMonster]);
        }
    }
    #Delete the Monster
    public function destroy($id)
    {
        $monster = Monster::findOrFail($id);
        $monster->delete();
        return response()-> json(["error" => false,"message" => "Monster was successfully deleted"]);
    }

    # Show the Maximum Number of Available Monsters
    public function totalMonsters()
    {
        $total = Monster::all()->count();
        return response()-> json(["total_monsters" => $total]);
    }
    
    # Show the most Popular Monster Race
    public function popularRace()
    {
        $popular = Monster::selectRaw('count(race_id) as total, race_id')->orderBy('total', 'desc')->groupBy('race_id')->limit(1)->firstOrFail();
        $pop_race_name = MonsterRace::find($popular['race_id'])['name'];
        return response()-> json(["popular_monster_race" => $pop_race_name]);
    }

    # Show the most Popular Monster Hability
    public function popularAbility()
    {
        $popular = MonsterAbility::selectRaw('count(power_id) as total, power_id')->orderBy('total', 'desc')->groupBy('power_id')->limit(1)->firstOrFail();
        $pop_hability_name = MonsterPower::find($popular['power_id'])['name'];
        return response()-> json(["popular_monster_power" => $pop_hability_name]);
    }

    #Private Functions
    private function validateMonster($request, $is_new)
    {   
        #If json on request is incorrect
        if (sizeof($request) == 0){
            return "Monster Parameters are incorrect";
        }

        #If is a new monster validate the level
        if ($is_new == true) {
            if ($request['level'] !== 1) {
                return "The level of a new monster must be 1";
            }
        }

        #The monster name cannot be empty
        if ($request['name'] == '') {
            return "The monster name cannot be empty";
        }

        #Validate the stats values
        if ($request['strength'] < 0 || $request['strength'] > 100) {
            return "Strength must be a value between 0 and 100";
        }

        if ($request['intelligence'] < 0 || $request['intelligence'] > 100) {
            return "Intelligence must be a value between 0 and 100";
        }

        if ($request['dexterity'] < 0 || $request['dexterity'] > 100) {
            return "Dexterity must be a value between 0 and 100";
        }

        return $this->validateAbilities($request);
    }

    private function validateAbilities($req)
    {
        $beholder_race =  $this->getRaceId("Beholder");
        $mind_flayer_race =  $this->getRaceId("Mind Flayer");
        $owlbear_race =  $this->getRaceId("Owlbear");
        $dragons_race =  $this->getRaceId("Dragons");
        $cloud_giant_race =  $this->getRaceId("Cloud Giant");
        $storm_giant_race =  $this->getRaceId("Storm Giant");
        $umber_hulk_race =  $this->getRaceId("Umber Hulk");
        $yuan_ti_race =  $this->getRaceId("Yuan-ti");
        $gelatinous_cube_race =  $this->getRaceId("Gelatinous Cube");
        $drow_race =  $this->getRaceId("Drow");
        $kobold_race =  $this->getRaceId("Kobold");

        $shadow_ball = $this->getPowerId("Shadow Ball");
        $aerial_ace = $this->getPowerId("Aerial Ace");
        $surf = $this->getPowerId("Surf");
        $double_team =$this->getPowerId("Double Team");
        $crunch = $this->getPowerId("Crunch");
        $giga_drain = $this->getPowerId("Giga Drain");

        #1.- Shadow Ball can only be used by Beholder and Mind Flayer
        if (in_array($shadow_ball, $req["abilities"])) {
            if ($req['race_id'] !== $beholder_race && $req['race_id'] !== $mind_flayer_race ) {
                return "Shadow Ball can only be used by Beholder and Mind Flayer";
            }
        }
        
        #2.- Aerial Ace can only be used by Owlbear, Dragons, Cloud Giant, Storm Giant,Umber Hulk
        if (in_array($aerial_ace, $req["abilities"])) {
            if ($req['race_id'] !== $owlbear_race && $req['race_id'] !== $dragons_race && $req['race_id'] !== $cloud_giant_race && $req['race_id'] !== $storm_giant_race && $req['race_id'] !== $umber_hulk_race ) 
            {
                return "Aerial Ace can only be used by Owlbear, Dragons, Cloud Giant, Storm Giant,Umber Hulk";
            }
        }

        #3.- Surf can only be used by Yuan-ti, Gelatinous Cube and Drow
        if (in_array($surf, $req["abilities"])) {

            if ($req['race_id'] !== $yuan_ti_race && $req['race_id'] !== $gelatinous_cube_race && $req['race_id'] !== $drow_race)
            {
                return "Surf can only be used by Yuan-ti, Gelatinous Cube and Drow";
            }

        }

        #4.- Kobold can only use Double Team and Crunch
        if ($req['race_id'] === $kobold_race){
            foreach ($req['abilities'] as $ability)  {
                if($ability !== $double_team || $ability !== $crunch)
                {
                    return "Kobold can only use Double Team and Crunch";
                    break;
                }
            }
        }

        #5.- Giga Drain can only be used by Mind Flyer
        if ( in_array($giga_drain, $req["abilities"]) ){
            if ($req['race_id'] !== $mind_flayer_race)
            {
                return "Giga Drain can only be used by Mind Flyer";
            }
        }

        return true;
    }


    private function getRaceId($name){
        return MonsterRace::select('id')->where('name', '=', "$name")->first()['id'];
    }

    private function getPowerId($name){
        return MonsterPower::select('id')->where('name', '=', "$name")->first()['id'];
    }
}
