<?php

namespace App\Http\Controllers;

use App\HeroLastName;
use Illuminate\Http\Request;

class HeroLastNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return HeroLastName::all();
    }

}
