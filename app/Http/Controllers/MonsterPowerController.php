<?php

namespace App\Http\Controllers;

use App\MonsterPower;
use Illuminate\Http\Request;

class MonsterPowerController extends Controller
{
    public function index()
    {
        return MonsterPower::all();
    }
}
