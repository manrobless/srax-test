<?php

namespace App\Http\Controllers;

use App\Hero;
use Illuminate\Http\Request;
use App\HeroRace;
use App\HeroClass;
use App\HeroWeapon;
use App\HeroFirstName;
use App\HeroLastName;

class HeroController extends Controller
{
    #Display a listing of Heroes
    public function index()
    {
        return Hero::all();
    }

    #Create a Hero
    public function store(Request $request)
    {
        $validHero = $this->validateHero($request->all(), true);

        if ($validHero === true){
            #Save the Hero
            return Hero::create($request->all());
        } else{
            return response()-> json(["error" => true,"description" => $validHero]);
        }

    }

    #Create a random hero
    public function generateRandom()
    {
        #Races
        $half_orc_race = $this->getRaceId("Half-orc");
        $dragonborn_race = $this->getRaceId("Dragonborn");
        $elf_race = $this->getRaceId("Elf");
        $dwarf_race = $this->getRaceId("Dwarf");
        
        #Random Data
        $rand_class = rand(1,7); #There are 7 classes
        $rand_race = rand(1,7); #There are 7 races
        $rand_weapon = rand(1,5); #There are 5 weapons

        $hero = [
            "class_id" => $rand_class,
            "race_id" => $rand_race,
            "weapon_id" => $rand_weapon
        ];

        
        #While not valid class with some race, try another random combination
        $validClass = $this->validateClass($hero, true);
        while (!$validClass === true){
            $rand_class = rand(1,7);
            $rand_race = rand(1,7);

            $hero = [
                "class_id" => $rand_class,
                "race_id" => $rand_race
            ];
            #Try again
            $validClass = $this->validateClass($hero, true);
        }

        #While not valid class with race, try another random weapon
        $validWeapon = $this->validateWeapon($hero);
        while (!$validClass === true){
            $rand_weapon = rand(1,5); 

            $hero = [
                "class_id" => $rand_class,
                "weapon_id" => $rand_weapon
            ];
            #Try again
            $validWeapon = $this->validateWeapon($hero);
        }

        $rand_first_name = rand(1,16); #There are 16 First Names
        $rand_last_name = rand(1,16); #There are 16 Last Names


        #1.-Dwarfs First and Last name must contain at least an “R” or an “H”
        if ($rand_race === $dwarf_race){
            $first_names = HeroFirstName::where('name', 'like', '%R%')->orWhere('name', 'like', '%H%')->get();
            $first_name = $first_names[rand(0, $first_names->count()-1 )]["name"];
            $last_names = HeroLastName::where('name', 'like', '%R%')->orWhere('name', 'like', '%H%')->get();
            $last_name = $last_names[rand(0, $last_names->count()-1 )]["name"];
        }else{
            $first_name = HeroFirstName::select('id','name')->where('id', '=', "$rand_first_name")->first()['name'];
            $last_name = HeroLastName::select('id','name')->where('id', '=', "$rand_last_name")->first()['name'];
        }
        

        #2.- Half-orcs and Dragonborns don’t have a Last Name
        if ($rand_race === $half_orc_race || $rand_race === $dragonborn_race){
            $last_name = "";
        }

        #3.-Elfs’ Last Name must be its First Name, but mirrored (i.e. Jimmy => Ymmij)
        if ($rand_race === $elf_race){
            $last_name = strrev($first_name);
        }

        #Generate a random hero after validations
        $random_hero = [
            "first_name" => $first_name,
            "last_name" => $last_name,
            "level" => 1,
            "race_id" => $rand_race,
            "class_id" => $rand_class,
            "weapon_id" => $rand_weapon,
            "strength" => rand(0,100),
            "intelligence" => rand(0,100),
            "dexterity" => rand(0,100)
        ];

        #Return the hero data to "fill" the "random hero form" in "view"
        return response()-> json($random_hero);
        
    }

    #Display the specified Heroe.
    public function show($id)
    {
        return Hero::find($id);
    }

    #Update the Hero.
    public function update(Request $request, $id)
    {
        $validHero = $this->validateHero($request->all(), false);

        if ($validHero === true){
            #Update the Hero
            $hero = Hero::findOrFail($id);
            $hero->update($request->all());
            return $hero;
        } else{
            return response()-> json(["error" => true,"description" => $validHero]);
        }
    }

    #Remove the Hero
    public function destroy($id)
    {
        $hero = Hero::findOrFail($id);
        $hero->delete();
        return response()-> json(["error" => false,"message" => "Hero was successfully deleted"]);
        
    }

    # Show the Maximum Number of Available Heroes
    public function totalHeroes(){
        $total = Hero::all()->count();
        return response()-> json(["total_heroes" => $total]);
    }
    
    # Show the most Popular Hero Race
    public function popularRace(){
        $popular = Hero::selectRaw('count(race_id) as total, race_id')->orderBy('total', 'desc')->groupBy('race_id')->limit(1)->firstOrFail();
        $pop_race_name = HeroRace::find($popular['race_id'])['name'];
        return response()-> json(["popular_hero_race" => $pop_race_name]);
    }

    # Show the most Popular Hero Class
    public function popularClass(){
        $popular = Hero::selectRaw('count(class_id) as total, class_id')->orderBy('total', 'desc')->groupBy('class_id')->limit(1)->firstOrFail();
        $pop_class_name = HeroClass::find($popular['class_id'])['name'];
        return response()-> json(["popular_hero_class" => $pop_class_name]);
    }

    # Show the most Popular Weapon
    public function popularWeapon(){
        $popular = Hero::selectRaw('count(weapon_id) as total, weapon_id')->orderBy('total', 'desc')->groupBy('weapon_id')->limit(1)->firstOrFail();
        $pop_weapon_name = HeroWeapon::find($popular['weapon_id'])['name'];
        return response()-> json(["popular_hero_weapon" => $pop_weapon_name]);
    }

    #Private Functions
    private function validateHero($request, $is_new)
    {   

        #If json on request is incorrect
        if (sizeof($request) == 0){
            return "Hero Parameters are incorrect";
        }

        #If is a new hero validate the level
        if ($is_new == true) {
            if ($request['level'] !== 1) {
                return "The level of a new hero must be 1";
            }
        }
        
        #Validate the stats values
        if ($request['strength'] < 0 || $request['strength'] > 100) {
            return "Strength must be a value between 0 and 100";
        }

        if ($request['intelligence'] < 0 || $request['intelligence'] > 100) {
            return "Intelligence must be a value between 0 and 100";
        }

        if ($request['dexterity'] < 0 || $request['dexterity'] > 100) {
            return "Dexterity must be a value between 0 and 100";
        }

        return $this->validateClass($request);
    }

    private function validateClass($request, $random = false){
        
        $human_race = $this->getRaceId("Human");
        $elf_race = $this->getRaceId("Elf");
        $half_elf_race = $this->getRaceId("Half-elf");
        $dwarf_race = $this->getRaceId("Dwarf");
        $halfling_race = $this->getRaceId("Halfling");
        $half_orc_race = $this->getRaceId("Half-orc");
        $dragonborn_race = $this->getRaceId("Dragonborn");
        
        $paladin_class = $this->getClassId("Paladin");
        $ranger_class = $this->getClassId("Ranger");
        $barbarian_class = $this->getClassId("Barbarian");
        $wizard_class = $this->getClassId("Wizard");
        $cleric_class = $this->getClassId("Cleric");
        $warrior_class = $this->getClassId("Warrior");

        #1.- Humans and Half-elves can only be Paladins
        if ($request['class_id'] === $paladin_class) {
            if ($request['race_id'] !== $human_race && $request['race_id'] !== $half_elf_race ) {
                return "Humans and Half-elves can only be Paladins";
            }
        }

        #2.- Dwarfs cannot be Rangers
        if ($request['race_id'] === $dwarf_race) {
            if ($request['class_id'] === $ranger_class) {
                return "Dwarfs cannot be Rangers";
            }
        }

        #3.- Elves, Half-elves and Halflings cannot be Barbarians
        if ($request['race_id'] === $elf_race || $request['race_id'] === $half_elf_race || $request['race_id'] === $halfling_race) {
            if ($request['class_id'] === $barbarian_class) {
                return "Elves, Half-elves and Halflings cannot be Barbarians";
            }
        }

        #4.-Half-orcs and Dwarfs cannot be Wizards
        if ($request['race_id'] === $half_orc_race || $request['race_id'] === $dwarf_race) {
            if ($request['class_id'] === $wizard_class) {
                return "Half-orcs and Dwarfs cannot be Wizards";
            }
        }

        #5.- Dragonborn, and Half-orcs cannot be Clerics
        if ($request['race_id'] === $dragonborn_race || $request['race_id'] === $half_orc_race) {
            if ($request['class_id'] === $cleric_class) {
                return "Dragonborn, and Half-orcs cannot be Clerics";
            }
        }

        #6.- Elfs cannot be Warriors
        if ($request['race_id'] === $elf_race['id']) {
            if ($request['class_id'] === $warrior_class['id']) {
                return "Elfs cannot be Warriors";
            }
        }

        if ($random == true){
            return true;
        }else{
            return $this->validateWeapon($request);
        }
    }

    private function validateWeapon($request){

        $paladin_class = $this->getClassId("Paladin");
        $ranger_class = $this->getClassId("Ranger");
        $barbarian_class = $this->getClassId("Barbarian");
        $wizard_class = $this->getClassId("Wizard");
        $thief_class = $this->getClassId("Thief");
        $cleric_class = $this->getClassId("Cleric");

        $sword = $this->getWeaponId("Sword");
        $staff = $this->getWeaponId("Staff");
        $hammer = $this->getWeaponId("Hammer");
        $bow_arrows = $this->getWeaponId("Bow and Arrows");

        #1.- Paladins can only use Swords
        if ($request['class_id'] === $paladin_class) {
            if ($request['weapon_id'] !== $sword) {
                return "Paladins can only use Swords";
            }
        }

        #2.- Rangers can only use Bow and Arrows
        if ($request['class_id'] === $ranger_class) {
            if ($request['weapon_id'] !== $bow_arrows) {
                return "Rangers can only use Bow and Arrows";
            }
        }

        #3.- Barbarian cannot use Bow and Arrows or Staffs
        if ($request['class_id'] === $barbarian_class) {
            if ($request['weapon_id'] === $bow_arrows || $request['weapon_id'] === $staff) {
                return "Barbarian cannot use Bow and Arrows or Staffs";
            }
        }

        #4.- Wizards and Clerics can only Use Staffs
        if ($request['class_id'] === $wizard_class || $request['class_id'] === $cleric_class) {
            if ($request['weapon_id'] !== $staff) {
                return "Wizards and Clerics can only Use Staffs";
            }
        }

        #5.-Thieves cannot use Hammers
        if ($request['class_id'] === $thief_class) {
            if ($request['weapon_id'] === $hammer) {
                return "Thieves cannot use Hammers";
            }
        }

        return true;
    }

    private function getClassId($name){
        return HeroClass::select('id')->where('name', '=', "$name")->first()['id'];
    }

    private function getRaceId($name){
        return HeroRace::select('id')->where('name', '=', "$name")->first()['id'];
    }

    private function getWeaponId($name){
        return HeroWeapon::select('id')->where('name', '=', "$name")->first()['id'];
    }



}
