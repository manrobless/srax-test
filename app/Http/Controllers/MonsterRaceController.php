<?php

namespace App\Http\Controllers;

use App\MonsterRace;
use Illuminate\Http\Request;

class MonsterRaceController extends Controller
{
    public function index()
    {
        return MonsterRace::all();
    }

}
