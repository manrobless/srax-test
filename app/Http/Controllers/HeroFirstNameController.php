<?php

namespace App\Http\Controllers;

use App\HeroFirstName;
use Illuminate\Http\Request;

class HeroFirstNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return HeroFirstName::all();
    }

}
