<?php

namespace App\Http\Controllers;

use App\HeroClass;
use Illuminate\Http\Request;

class HeroClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return HeroClass::all();
    }
}
