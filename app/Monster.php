<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Monster extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $hidden = ['deleted_at'];
    protected $fillable = ['name','picture','level','race_id','strength','intelligence','dexterity'];

    protected $casts = [
        'picture' => 'array'
    ];

    public function abilities()
    {
        return $this->hasMany('App\MonsterAbility');
    }
}
